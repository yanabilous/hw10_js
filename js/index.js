function clickAction(event) {
    if (event.target.closest(".tabs")) {

        document.querySelectorAll(".tabs li").forEach(el => {
            el.classList.remove("active");
        });
        event.target.classList.add("active");

        let tabId = event.target.dataset.tabid;

        console.log(tabId);
        if (tabId) {
            document.querySelectorAll(".tabs-content > li").forEach((el, index) => {
                el.style.display = "none";
            });

            document.getElementById(tabId).style.display = "block";

        }
    }
}

document.addEventListener("click", clickAction);

document.querySelectorAll(".tabs-content > li").forEach((el, index) => {
    if (index !== 0) {
        el.style.display = "none";
    }

});




